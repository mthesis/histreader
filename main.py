import PIL
from PIL import Image
import numpy as np

import matplotlib.pyplot as plt

smoothing=3
filter2steps=1

image=Image.open("crop.jpg")

data=np.copy(np.asarray(image))
# print(data.shape)

def filtercyan(q):
  qq=abs(q[0]/2-q[2]/2)
  return [qq for i in range(3)]

for i in range(len(data)):
  for j in range(len(data[i])):
    data[i,j]=filtercyan(data[i,j])
    data[i,j,0]=0
    # data[i,j,0]=255

did=np.mean(data,axis=-1)
didp=np.copy(did)

for smo in range(smoothing):
  print(f"smoothing for the {smo+1} time")
  did=np.copy(didp)
  for i in range(1,len(didp)-1):
    for j in range(1,len(didp[i])-1):
      didp[i,j]=did[i,j]+did[i+1,j]+did[i-1,j]+did[i,j+1]+did[i,j-1]


di2=np.argmax(didp,axis=0)

for i,di in enumerate(di2):
  data[di,i,0]=255


print("showing in Image")
img=Image.fromarray(data)
img.show()


minpx=65#minum index pixel x
minx=0.5
maxx=2.5

maxpy=110
miny=0
maxy=-3
#also here y log axis always


numy=int(data.shape[0])
numx=int(data.shape[1])

dx=numx-minpx
dy=maxpy

dxdp=(maxx-minx)/(dx-1)
dydp=(maxy-miny)/(dy)

pxtox={i:dxdp*(i-minpx)+minx for i in range(numx)}

pytoy={i:10**(dydp*i) for i in range(numy)}


rx,ry=np.transpose([(pxtox[i],pytoy[d]) for i,d in enumerate(di2)])

# print(rx.shape,ry.shape)

plt.plot(rx,ry,"o")
# plt.title("unfiltered")
plt.yscale("log",nonposy="clip")

plt.savefig("output.png",format="png")
plt.savefig("output.pdf",format="pdf")

plt.show()

np.savez_compressed("output",x=rx,y=ry)

#the following code does not work, so its disabled
exit()

#or filtered more

for filt in range(filter2steps):
  print(f"subfiltering for the {filt+1} time")
  ry2=np.copy(ry)

  for i in range(len(ry2)):
    ac=ry[i]
    acm=ry2[i-1] if i>0 else ac
    acp=ry[i+1] if i<len(ry)-1 else ac
    
    if ac-np.mean([acp,acm])>np.abs(acp-acm):
      ry2[i]=acm#np.mean([acp,acm])
  ry=ry2

# np.savez_compressed("filtered",x=rx,y=ry)
  

plt.plot(rx,ry2,"o")
plt.title("filtered")
plt.show()

  













